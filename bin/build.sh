#!/usr/bin/env bash

set -e

CI_PAGES_DOMAIN=${1}
CI_PAGES_URL=${2}
CI_PROJECT_TITLE=${3}
CI_PROJECT_URL=${4}
COMMIT_TIME=${5}
GITLAB_USER_NAME=${6}
GITLAB_USER_EMAIL=${7}
CI_COMMIT_SHA=${8}
CI_PROJECT_VISIBILITY=${9}

export PATH=$HOME/.cabal/bin:$HOME/.ghcup/bin:$PATH

if ! type -p xelatex >/dev/null ; then
  >&2 echo "Missing: xelatex" >&2
fi

if ! type -p pandoc >/dev/null ; then
  >&2 echo "Missing: pandoc" >&2
fi

if ! type -p gs >/dev/null ; then
  >&2 echo "Missing: ghostscript" >&2
fi

if ! type -p convert >/dev/null ; then
  >&2 echo "Missing: imagemagick" >&2
fi

if ! type -p libreoffice >/dev/null ; then
  >&2 echo "Missing: libreoffice" >&2
fi

if ! type -p rsync >/dev/null ; then
  >&2 echo "Missing: rsync" >&2
fi

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
share_dir=${script_dir}/../share

mkdir -p ${dist_dir}

function substituteForm () {
  INPUT_FILE=${1}
  OUTPUT_DIR=${2}

  INPUT_FILE_basename=$(basename "$INPUT_FILE")
  INPUT_FILE_filename=${INPUT_FILE_basename%.*}
  INPUT_FILE_extension=${INPUT_FILE##*.}

  OUTPUT_FILE=${OUTPUT_DIR}/${INPUT_FILE_filename}.${INPUT_FILE_extension}

  cat ${INPUT_FILE} | \
  sed -e "s#\${CI_PAGES_DOMAIN}#${CI_PAGES_DOMAIN}#g" \
      -e "s#\${CI_PAGES_URL}#${CI_PAGES_URL}#g" \
      -e "s#\${CI_PROJECT_TITLE}#${CI_PROJECT_TITLE}#g" \
      -e "s#\${CI_PROJECT_URL}#${CI_PROJECT_URL}#g" \
      -e "s#\${COMMIT_TIME}#${COMMIT_TIME}#g" \
      -e "s#\${GITLAB_USER_NAME}#${GITLAB_USER_NAME}#g" \
      -e "s#\${GITLAB_USER_EMAIL}#${GITLAB_USER_EMAIL}#g" \
      -e "s#\${CI_COMMIT_SHA}#${CI_COMMIT_SHA}#g" \
      -e "s#\${CI_PROJECT_VISIBILITY}#${CI_PROJECT_VISIBILITY}#g" \
      > ${OUTPUT_FILE}

  OUTPUT_FILENAME=$(realpath --relative-to=${dist_dir} ${OUTPUT_FILE})
  echo ${OUTPUT_FILENAME}
}

function substituteMarkdown () {
  input_file=$1

  input_file_relative=$(realpath --relative-to=${src_dir} ${input_file})
  input_file_relative_basename=$(basename ${input_file_relative})
  input_file_relative_dirname=$(dirname ${input_file_relative})

  dist_dir_relative=${dist_dir}/${input_file_relative_dirname}
  mkdir -p ${dist_dir_relative}

  (
    echo -e "# ${CI_PROJECT_TITLE}" && \
    echo -e "\n----\n" && \
    cat ${input_file} && \
    echo -e "\n----\n" && \
    echo -e "### This document\n\n" && \
    echo -e "* hosted at **[${CI_PAGES_URL}](${CI_PAGES_URL})**\n" && \
    echo -e "* source hosted at **[${CI_PROJECT_URL}](${CI_PROJECT_URL})**\n" && \
    echo -e "* last updated at time **${COMMIT_TIME}**\n" && \
    echo -e "* last updated by user **[${GITLAB_USER_NAME}](mailto:${GITLAB_USER_EMAIL})**\n" && \
    echo -e "* revision **[${CI_COMMIT_SHA}](${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA})**\n" && \
    echo -e "* access control **${CI_PROJECT_VISIBILITY}**\n" && \
    echo -e "\n----\n" && \
    cat ${share_dir}/licence.md &&\
    echo -e "\n----\n"
  ) > ${dist_dir_relative}/${input_file_relative_basename}

  output=$(realpath --relative-to=${dist_dir} ${dist_dir}/${input_file_relative_dirname}/${input_file_relative_basename})
}

function makeLibreOfficeOutputs () {
  formats=$1
  libre_files=$(find -L ${dist_dir} -name '*.fodg' -o -name '*.fodt' -o -name '*.fods' -o -name '*.odg' -o -name '*.odt' -type f | sort)

  for libre_file in ${libre_files}; do
    libre_file_relative=$(realpath --relative-to=${dist_dir} ${libre_file})
    libre_file_relative_dirname=$(dirname ${libre_file_relative})
    libre_file_basename=$(basename -- "${libre_file}")
    libre_file_filename="${libre_file_basename%.*}"
    libre_file_extension="${libre_file_relative##*.}"

    dist_dir_relative=${dist_dir}/${libre_file_relative_dirname}

    for format in ${formats}; do
      libreoffice --invisible --headless --convert-to ${format} ${libre_file} --outdir ${dist_dir_relative}
      output=$(realpath --relative-to=${dist_dir} ${dist_dir}/${libre_file_relative_dirname}/${libre_file_filename}.${format})
    done
  done
}

function makeMarkdownOutputs () {
  formats=$1
  md_files=$(find -L ${dist_dir} -name '*.md' -type f | sort)

  for md_file in ${md_files}; do
    md_file_relative=$(realpath --relative-to=${dist_dir} ${md_file})
    md_file_relative_dirname=$(dirname ${md_file_relative})
    md_file_basename=$(basename -- "${md_file}")
    md_file_filename="${md_file_basename%.*}"

    dist_dir_relative=${dist_dir}/${md_file_relative_dirname}

    for format in ${formats}; do
      output=${dist_dir}/${md_file_relative_dirname}/${md_file_filename}.${format}
      mkdir -p ${dist_dir}/${md_file_relative_dirname}
      pandoc -fmarkdown-implicit_figures -M mainfont="DejaVu Sans Mono" --pdf-engine=xelatex ${md_file} -o ${output}
      echo ${output} >> /tmp/blah.txt
      output_relative=$(realpath --relative-to=${dist_dir} ${output})
    done
  done
}

function preProcessSource () {
  form_extensions=$1
  pre_src_files=$(find -L ${src_dir} -type f | sort)

  for pre_src_file in ${pre_src_files}
  do
    pre_src_file_relative=$(realpath --relative-to=${src_dir} ${pre_src_file})
    pre_src_file_relative_dirname=$(dirname ${pre_src_file_relative})
    pre_src_file_relative_basename=$(basename -- "${pre_src_file_relative}")
    pre_src_file_extension="${pre_src_file_relative##*.}"

    mkdir -p ${dist_dir}/${pre_src_file_relative_dirname}

    is_form="0"
    for form_extension in ${form_extensions}
    do
      if [ ${pre_src_file_extension} == ${form_extension} ]
      then
        is_form="1"
        break
      fi
    done

    if  [ $is_form == "1" ]
    then
      substituteForm "${pre_src_file}" "${dist_dir}/${pre_src_file_relative_dirname}"
    else
    if  [ ${pre_src_file_extension} = "md" ]; then
      substituteMarkdown "${pre_src_file}" "${dist_dir}/${pre_src_file_relative_dirname}"
    else
      output=${dist_dir}/${pre_src_file_relative_basename}
      rsync -aH ${pre_src_file} ${dist_dir}
    fi
  fi
  done
}

function makePdfPage1 () {
  pdf_files=$(find -L ${dist_dir} -name '*.pdf' -type f | sort)

  for pdf_file in ${pdf_files}; do
    makePage1 ${pdf_file}
  done
}

function makePage1 () {
  pdf_file=${1}
  pdf_basename=$(basename -- "${pdf_file}")
  pdf_dirname=$(dirname -- "${pdf_file}")
  pdf_filename="${pdf_basename%.*}"

  page1_pdf_dir=${pdf_dirname}
  page1_pdf="${page1_pdf_dir}/${pdf_filename}_page1.pdf"

  mkdir -p "${page1_pdf_dir}"

  gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${pdf_file}
  page1_pdf_relative=$(realpath --relative-to=${dist_dir} ${page1_pdf})

  page1_pdf_png="${page1_pdf}.png"
  convert ${page1_pdf} ${page1_pdf_png}
}

function makeImageSizes () {
  sizes=$1
  img_files=$(find -L ${dist_dir} -name '*.png' -o -name '*.jpg' -o -name '*.gif' -type f | sort)

  for img_file in ${img_files}; do
    img_file_extension="${img_file##*.}"

    for size in ${sizes}
    do
      img_sized_file="${img_file}-${size}.${img_file_extension}"
      convert ${img_file} -resize ${size}x${size} ${img_sized_file}
      img_sized_file_relative=$(realpath --relative-to=${dist_dir} ${img_sized_file})
    done
  done
}

preProcessSource "fodt fodg fods txt html"
makeLibreOfficeOutputs "pdf docx"
makeMarkdownOutputs "pdf odt"

rsync -aH ${share_dir}/ ${dist_dir}

makePdfPage1
makeImageSizes "1200 450 150 65"
